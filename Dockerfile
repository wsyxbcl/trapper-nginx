FROM nginx:alpine

ARG SERVICE_TEMPLATE=ssl.conf.template

RUN apk add --update make openssl
RUN mkdir /etc/nginx/templates/
COPY templates/$SERVICE_TEMPLATE /etc/nginx/templates/default.conf.template
COPY nginx.conf /etc/nginx/nginx.conf
